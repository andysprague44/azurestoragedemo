﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AzureDemoUploadApp.Services;
using NDesk.Options;

namespace AzureDemoUploadApp
{
    public class Program
    {


        public static int Main(string[] args)
        {
            var accountName = string.Empty;
            var accountKey = string.Empty;
            var filePath = string.Empty;
            var containerName = string.Empty;
            var blobName = string.Empty;
            var isCompressed = false;
            var showHelp = false;

            var optionSet = new OptionSet
            {
                {"accountName=", "the storage account name", n => accountName = n},
                {"accountKey=", "the storage account key", k => accountKey = k},
                {"sourceFile=", "the full path of the file to upload", f => filePath = f},
                {"container=", "the azure blob store container name", c => containerName = c},
                {"destinationBlob=", "the location of the blob within the container", b => blobName = b},
                {"isCompressed", "is the source already compressed", z => isCompressed = z != null},
                {"help", "show this message and exit", v => showHelp = v != null}
            };

            optionSet.Parse(args);

            if (showHelp)
            {
                optionSet.WriteOptionDescriptions(Console.Out);
                return 1;
            }
            
            var uploadService = new AzureUploadService(accountName, accountKey);

            var sourceFile = new FileInfo(filePath);
            
            var compressedFile = new FileInfo($@"{sourceFile.DirectoryName}\out\{sourceFile.Name}.gz");

            compressedFile = isCompressed
                ? sourceFile
                : uploadService.CompressFile(sourceFile, compressedFile);

            uploadService.UploadFileToBlobStore(compressedFile, containerName, blobName);

            return 0;
        }
    }
}
