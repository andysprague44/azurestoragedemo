﻿using System.IO;

namespace AzureDemoUploadApp.Services
{
    public interface IAzureUploadService
    {
        FileInfo CompressFile(FileInfo sourceFile, FileInfo compressedFile);
        int UploadFileToBlobStore(FileInfo compressedFile, string containerName, string blobName);
    }
}