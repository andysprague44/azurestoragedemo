﻿using System;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Text;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using Microsoft.WindowsAzure.Storage.RetryPolicies;

namespace AzureDemoUploadApp.Services
{
    public class AzureUploadService : IAzureUploadService
    {
        private const string OriginalContentType = "text/csv";
        private const string CompressedContentType = "application/gzip";
        private const string CacheControl = "public, no-store";
        private readonly TimeSpan _executionTimeout = TimeSpan.FromMinutes(5);

        private readonly CloudBlobClient _cloudBlobClient;

        public AzureUploadService(string accountName, string accountKey)
        {
            var blobStorageConnectionString =
                $"DefaultEndpointsProtocol=https;AccountName={accountName};AccountKey={accountKey}";
            var account = CloudStorageAccount.Parse(blobStorageConnectionString);

            _cloudBlobClient = new CloudBlobClient(account.BlobEndpoint, account.Credentials);
//            var requestOptions = new BlobRequestOptions
//            {
//                MaximumExecutionTime = _executionTimeout,
//                RetryPolicy = new LinearRetry(TimeSpan.FromSeconds(2), 1)
//            };
//            _cloudBlobClient.DefaultRequestOptions = requestOptions;
        }

        public FileInfo CompressFile(FileInfo sourceFile, FileInfo compressedFile)
        {
            return LogTimeElapsed($"Compressing file {sourceFile.Name} ({FileSize(sourceFile)})", () =>
            {
                if (!FileEncoding(sourceFile).Equals(Encoding.UTF8))
                {
                    throw new Exception("Source file must be UTF8 encoded");
                }

                compressedFile.Directory?.Create();
                compressedFile.Delete();

                using (var inFile = new FileStream(sourceFile.FullName, FileMode.Open, FileAccess.Read))
                using (
                    var outFile = new FileStream(compressedFile.FullName, FileMode.Create, FileAccess.Write,
                        FileShare.Read))
                using (var gZipStream = new GZipStream(outFile, CompressionMode.Compress))
                {
                    inFile.CopyTo(gZipStream);
                }
                compressedFile.Refresh();
                return compressedFile;
            });
        }

        public int UploadFileToBlobStore(FileInfo compressedFile, string containerName, string blobName)
        {
            return LogTimeElapsed($"Uploading file {compressedFile.Name} ({FileSize(compressedFile)})", () =>
            {
                var container = _cloudBlobClient.GetContainerReference(containerName);
                if (!container.Exists())
                {
                    container.Create(BlobContainerPublicAccessType.Blob);
                }

                var blob = container.GetBlockBlobReference(blobName);
                blob.Properties.ContentEncoding = CompressedContentType;
                blob.Properties.ContentType = OriginalContentType;
                blob.Properties.CacheControl = CacheControl;

                using (var fileStream = compressedFile.OpenRead())
                {
                    blob.UploadFromStream(fileStream);
                }
                return 0;
            });
        }

        private static Encoding FileEncoding(FileInfo fileInfo)
        {
            using (var fileStream = new FileStream(fileInfo.FullName, FileMode.Open, FileAccess.Read))
            using (var streamReader = new StreamReader(fileStream))
            {
                streamReader.Peek(); // you need this!
                return streamReader.CurrentEncoding;
            }
        }

        private static string FileSize(FileInfo fileInfo)
        {
            double size = fileInfo.Length;
            var units = new[] { "B", "KB", "MB", "GB", "TB" };
            var index = 0;
            while (size >= 1024 && index < 4)
            {
                size = size / 1024.0;
                index++;
            }

            return $"{size:0.0#}{units[index]}";
        }

        private static T LogTimeElapsed<T>(string message, Func<T> code)
        {
            Console.WriteLine($"Starting: {message}");
            var stopwatch = new Stopwatch();
            stopwatch.Start();

            try
            {
                var result = code();

                stopwatch.Stop();
                Console.WriteLine($"Completed: {message}. Elapsed Time: {stopwatch.Elapsed}");

                return result;
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Failed: {message}. Elapsed Time: {stopwatch.Elapsed}");
                Console.WriteLine(ex.Message);
                return default(T);
            }
        }


    }
}