
#AzureDemoUploadApp

This is a small utility program to upload a csv file to Azure Blob Storage.

It takes an uncompressed csv file, gzips it, and uploads to the specified blob destination.

##How To Use

An example of the cmd line required to run:

```
--accountName=nephiladev42 --accountKey=+UOgXrKsi1wzNOCKuXDcevoNvfm+/gettherealkeyfromazureportal== --file="E:\Data\123456.csv" --container=somecontainer --blob=somesubdir/123456.csv.gz --isCompressed
```

I
Get the azure storage account name/key from the [azure portal](portal.azure.com): Storage accounts -> choose account -> Settings: Access Keys

##Azure CLI alternative

Can achieve the above in the same way using Azure CLI - download from https://docs.microsoft.com/en-us/cli/azure/install-azure-cli

1. Set env variables for AZURE_STORAGE_ACCOUNT and AZURE_STORAGE_ACCESS_KEY:  `$env:VariableName = 'new-value'`

Query examples:
```
azure storage --help
azure storage container list
azure storage blob upload --container somecontainer --file .\123457.csv.gz --blob somesubdir/123457.csv.gz
```

This doesn't gzip it of course but I'm sure you can work that out.

##Load into SQWL DW

Using Polybase this azure blob can then be loaded into Azure SQL DW

```
--Create a credential to access blob store from SQL DW
CREATE DATABASE SCOPED CREDENTIAL [stg].[DemoCredential]
WITH IDENTITY = '[Azure storage account name]',
SECRET = '[Azure storage account key]'

CREATE EXTERNAL DATA SOURCE DemoDataSource
WITH (
    TYPE = HADOOP,
    LOCATION = 'wasbs://democontainer@[storage account].blob.core.windows.net/', --[container]@[storage account]
    CREDENTIAL = DemoCredential
);

CREATE EXTERNAL FILE FORMAT DemoFileFormat
WITH (
	FORMAT_TYPE = DELIMITEDTEXT,
	FORMAT_OPTIONS (
		FIELD_TERMINATOR = ',',
		STRING_DELIMITER = '"',
		DATE_FORMAT = 'yyyy-MM-dd HH:mm:ss.fffffff',
		USE_TYPE_DEFAULT = FALSE
	),
	DATA_COMPRESSION = 'org.apache.hadoop.io.compress.GzipCodec'
);

CREATE EXTERNAL TABLE DemoExternalTable
(
	[Col1] [bigint] NOT NULL,
	[Col2] [smallint] NOT NULL,
	[Col3] [smallint] NOT NULL,
	[Col4] [smallint] NOT NULL,
	[Col5] [bigint] NOT NULL,
	[Col6] [int] NOT NULL,
	[Col7] [smallint] NOT NULL,
	[Col8] [bit] NOT NULL,
	[Col9] [smallint] NOT NULL,
	[Col10] [numeric](23, 19) NOT NULL,
	[Col11] [numeric](23, 19) NOT NULL
)
WITH (
     DATA_SOURCE	= DemoDataSource
	,LOCATION		= 'somesubdir/' --can be directory or single file
	,FILE_FORMAT	= DemoFileFormat
	,REJECT_TYPE	= VALUE
	,REJECT_VALUE	= 0
);

SELECT COUNT(*) from DemoExternalTable

CREATE TABLE DemoPhysicalTable
WITH
(
	CLUSTERED COLUMNSTORE INDEX,
	DISTRIBUTION = HASH([Col1])
)
AS
SELECT * FROM DemoExternalTable;

SELECT COUNT(*) from DemoPhysicalTable
```

